package ratadie

import (
	"testing"
	"time"
)

type rdtest struct {
	year  int
	month time.Month
	day   int
	wd    time.Weekday
	rd    Day
}

var rdtests = []rdtest{
	// Epochs of various calendars
	{-4713, 11, 21, time.Friday, Day(-1721428)},
	{-3760, 9, 7, time.Monday, Day(-1373427)},
	{-3113, 8, 11, time.Monday, Day(-1137142)},
	{-3101, 1, 23, time.Friday, Day(-1132959)},
	{-2636, 2, 15, time.Wednesday, Day(-963099)},
	{-746, 2, 18, time.Wednesday, Day(-272787)},
	{-127, 12, 7, time.Sunday, Day(-46410)},
	{0, 12, 30, time.Saturday, Day(-1)},
	{0, 12, 31, time.Sunday, Day(0)},
	{1, 1, 1, time.Monday, Day(1)},
	{8, 8, 27, time.Wednesday, Day(2796)},
	{284, 8, 29, time.Friday, Day(103605)},
	{552, 7, 13, time.Thursday, Day(201443)},
	{622, 3, 22, time.Friday, Day(226896)},
	{622, 7, 19, time.Friday, Day(227015)},
	{632, 6, 19, time.Tuesday, Day(230638)},
	{1792, 9, 22, time.Saturday, Day(654415)},
	{1844, 3, 21, time.Thursday, Day(673222)},
	{1858, 11, 17, time.Wednesday, Day(678576)},
	// The 33 sample dates from Calendrical Calculations Appendix C
	{-586, 7, 24, time.Sunday, Day(-214193)},
	{-168, 12, 5, time.Wednesday, Day(-61387)},
	{70, 9, 24, time.Wednesday, Day(25469)},
	{135, 10, 2, time.Sunday, Day(49217)},
	{470, 1, 8, time.Wednesday, Day(171307)},
	{576, 5, 20, time.Monday, Day(210155)},
	{694, 11, 10, time.Saturday, Day(253427)},
	{1013, 4, 25, time.Sunday, Day(369740)},
	{1096, 5, 24, time.Sunday, Day(400085)},
	{1190, 3, 23, time.Friday, Day(434355)},
	{1240, 3, 10, time.Saturday, Day(452605)},
	{1288, 4, 2, time.Friday, Day(470160)},
	{1298, 4, 27, time.Sunday, Day(473837)},
	{1391, 6, 12, time.Sunday, Day(507850)},
	{1436, 2, 3, time.Wednesday, Day(524156)},
	{1492, 4, 9, time.Saturday, Day(544676)},
	{1553, 9, 19, time.Saturday, Day(567118)},
	{1560, 3, 5, time.Saturday, Day(569477)},
	{1648, 6, 10, time.Wednesday, Day(601716)},
	{1680, 6, 30, time.Sunday, Day(613424)},
	{1716, 7, 24, time.Friday, Day(626596)},
	{1768, 6, 19, time.Sunday, Day(645554)},
	{1819, 8, 2, time.Monday, Day(664224)},
	{1839, 3, 27, time.Wednesday, Day(671401)},
	{1903, 4, 19, time.Sunday, Day(694799)},
	{1929, 8, 25, time.Sunday, Day(704424)},
	{1941, 9, 29, time.Monday, Day(708842)},
	{1943, 4, 19, time.Monday, Day(709409)},
	{1943, 10, 7, time.Thursday, Day(709580)},
	{1992, 3, 17, time.Tuesday, Day(727274)},
	{1996, 2, 25, time.Sunday, Day(728714)},
	{2038, 11, 10, time.Wednesday, Day(744313)},
	{2094, 7, 18, time.Sunday, Day(764652)},
	// Some negative leap days and non-leap days
	{-4, 2, 28, time.Wednesday, Day(-1768)},
	{-4, 2, 29, time.Thursday, Day(-1767)},
	{-4, 3, 1, time.Friday, Day(-1766)},
	{-100, 2, 28, time.Wednesday, Day(-36831)},
	{-100, 3, 1, time.Thursday, Day(-36830)},
	{-400, 2, 28, time.Monday, Day(-146404)},
	{-400, 2, 29, time.Tuesday, Day(-146403)},
	{-400, 3, 1, time.Wednesday, Day(-146402)},
	{-5879608, 2, 28, time.Friday, Day(-2147483032)},
	{-5879608, 2, 29, time.Saturday, Day(-2147483031)},
	{-5879608, 3, 1, time.Sunday, Day(-2147483030)},
	// Extreme cases
	{-5879610, 6, 22, time.Friday, Day(-2147483648)},
	{-5879610, 6, 23, time.Saturday, Day(-2147483647)},
	{5879611, 7, 10, time.Sunday, Day(2147483646)},
	{5879611, 7, 11, time.Monday, Day(2147483647)},
	// Count across a year boundary at the high and low end.
	{-5879610, 7, 1, time.Sunday, Day(-2147483648 + 9)},
	{-5879610, 8, 1, time.Wednesday, Day(-2147483648 + 9 + 31)},
	{-5879610, 9, 1, time.Saturday, Day(-2147483648 + 9 + 31 + 31)},
	{-5879610, 10, 1, time.Monday, Day(-2147483648 + 9 + 31 + 31 + 30)},
	{-5879610, 11, 1, time.Thursday, Day(-2147483648 + 9 + 31 + 31 + 30 + 31)},
	{-5879610, 12, 1, time.Saturday, Day(-2147483648 + 9 + 31 + 31 + 30 + 31 + 30)},
	{-5879609, 1, 1, time.Tuesday, Day(-2147483648 + 9 + 31 + 31 + 30 + 31 + 30 + 31)},
	{-5879609, 2, 1, time.Friday, Day(-2147483648 + 9 + 31 + 31 + 30 + 31 + 30 + 31 + 31)},
	{5879611, 7, 1, time.Friday, Day(2147483647 - 10)},
	{5879611, 6, 1, time.Wednesday, Day(2147483647 - 10 - 30)},
	{5879611, 5, 1, time.Sunday, Day(2147483647 - 10 - 30 - 31)},
	{5879611, 4, 1, time.Friday, Day(2147483647 - 10 - 30 - 31 - 30)},
	{5879611, 3, 1, time.Tuesday, Day(2147483647 - 10 - 30 - 31 - 30 - 31)},
	{5879611, 2, 1, time.Tuesday, Day(2147483647 - 10 - 30 - 31 - 30 - 31 - 28)},
	{5879611, 1, 1, time.Saturday, Day(2147483647 - 10 - 30 - 31 - 30 - 31 - 28 - 31)},
	{5879610, 12, 1, time.Wednesday, Day(2147483647 - 10 - 30 - 31 - 30 - 31 - 28 - 31 - 31)},
	{5879610, 11, 1, time.Monday, Day(2147483647 - 10 - 30 - 31 - 30 - 31 - 28 - 31 - 31 - 30)},
}

func TestDay(t *testing.T) {
	for _, c := range rdtests {
		rd := Date(c.year, c.month, c.day)
		if rd != c.rd {
			t.Errorf("Bad conversion to Day: %d-%d-%d -> %d", c.year, c.month, c.day, rd)
		}
		y, m, d := c.rd.Date()
		if y != c.year || m != c.month || d != c.day {
			t.Errorf("Bad conversion from Day: %d -> %d-%d-%d", c.rd, y, m, d)
		}
		if c.wd != c.rd.Weekday() {
			t.Errorf("wrong Weekday for Day(%d): expecting %v got %v", c.rd, c.wd, c.rd.Weekday())
		}
	}
}

func TestTextMarshaler(t *testing.T) {
	for _, c := range rdtests {
		b, err := c.rd.MarshalText()
		if err != nil {
			t.Errorf("marshaling Day(%d): %v", c.rd, err)
			continue
		}
		var rd Day
		err = rd.UnmarshalText(b)
		if err != nil {
			t.Errorf("unmarshaling Day(%d) %q: %v", c.rd, string(b), err)
			continue
		}
		if rd != c.rd {
			t.Error("bad marshaling round-trip; got", rd, "want", c.rd)
		}
	}
}