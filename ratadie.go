// Package ratadie represents calendar dates as fixed integers, as specified
// in Dershowitz and Reingold's Calendrical Calculations.
package ratadie

import (
	"fmt"
	"strconv"
	"time"
)

// A Day represents a given calendar date as a fixed
// integer, as specified in Dershowitz and Reingold's
// Calendrical Calculations. Day(1) is Jan 1 of year 1
// in the proleptic Gregorian calendar. Representable
// dates range from Jun 22, -5879610 to Jul 11, 5879611.
type Day int32

const (
	daysIn1Year = 365
	// There's 1 leap day in a normal 4-year cycle.
	daysIn4Years = 4*daysIn1Year + 1
	// One leap day is missed (on the century year)
	// in a normal 100-year cycle.
	daysIn100Years = 25*daysIn4Years - 1
	// Century years divisible by 400 have a leap day,
	// so there's one extra in a 400-year cycle.
	daysIn400Years = 4*daysIn100Years + 1
)

const (
	int32min = -2147483648
	int32max = 2147483647
	// Even though Days are int32s, all conversions are
	// done using positive int64s so we don't have to worry
	// about overflow, underflow, or the difference between
	// floor division and truncated division.
	offsetYear = 6000000
	offsetDay  = offsetYear / 400 * daysIn400Years
)

// Date returns the Day for the given year, month, and day.
// The result of passing out-of-range month or day values is undefined.
// Do not use this function for month arithmetic.
func Date(year int, month time.Month, day int) Day {
	return Day(date(year, month, day) - offsetDay)
}

func date(year int, month time.Month, day int) int64 {
	y0 := int64(year) + offsetYear - 1
	d := 365*y0 + y0/4 - y0/100 + y0/400 + ((367*int64(month) - 362) / 12) + int64(day)
	// Correct for the assumption above that February has 30 days.
	if int(month) > 2 {
		if isLeapYear(year) {
			d -= 1
		} else {
			d -= 2
		}
	}
	return d
}

// Time is equivalent to calling Date(t.Date()).
// Note that time and location are completely ignored.
func Time(t time.Time) Day {
	return Date(t.Date())
}

// Today returns today's date. It is equivalent to Time(time.Now()).
func Today() Day {
	return Time(time.Now())
}

// Parse parses a formatted string and returns the day
// value it represents. It is a convenience wrapper
// around time.Parse.
//
// Note that time and location may be specified in the
// format, but are completely ignored.
func Parse(layout, value string) (Day, error) {
	t, err := time.Parse(layout, value)
	if err != nil {
		return 0, err
	}
	return Time(t), nil
}

// Date returns the Gregorian year, month, and day for rd.
func (rd Day) Date() (year int, month time.Month, day int) {
	d0 := int64(rd) + offsetDay - 1
	n400, d1 := d0/daysIn400Years, d0%daysIn400Years
	n100, d2 := d1/daysIn100Years, d1%daysIn100Years
	n4, d3 := d2/daysIn4Years, d2%daysIn4Years
	n1, prior := d3/daysIn1Year, d3%daysIn1Year
	year = int(400*n400 + 100*n100 + 4*n4 + n1 - offsetYear)
	if n1 == 4 || n100 == 4 {
		// The final day of a leap year.
		prior = 365
	} else {
		// We always end up with one less than the year number,
		// except in the final day of a leap year.
		year++
	}
	isLeap := isLeapYear(year)
	// Correct for the assumption below that February has 30 days.
	if prior < 31+28 || isLeap && prior == 31+28 {
		// Before March 1, no correction is needed.
	} else if isLeap {
		// 29 days in a leap year.
		prior += 1
	} else {
		// 28 days otherwise
		prior += 2
	}
	month = time.Month((12*prior + 373) / 367)
	day = 2 + int(d0-date(year, month, 1))
	return
}

// Weekday returns the day of the week specified by rd.
func (rd Day) Weekday() time.Weekday {
	return time.Weekday((int64(rd) + offsetDay) % 7)
}

// TimeIn returns a time value for midnight in the given location on the day represented by rd.
func (rd Day) TimeIn(loc *time.Location) time.Time {
	y, m, d := rd.Date()
	return time.Date(y, m, d, 0, 0, 0, 0, loc)
}

// Time returns a UTC time value for midnight on the day represented by rd.
func (rd Day) Time() time.Time { return rd.TimeIn(time.UTC) }

// Format is just a convenience for rd.Time().Format(layout)
func (rd Day) Format(layout string) string { return rd.Time().Format(layout) }

// MarshalText implements encoding.TextMarshaler.
func (rd Day) MarshalText() ([]byte, error) {
	y, m, d := rd.Date()
	s := fmt.Sprintf("%d-%02d-%02d", y, m, d)
	return []byte(s), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (rd *Day) UnmarshalText(b []byte) error {
	s := string(b)
	if len(s) < 7 {
		return unmarshalErr(s)
	}
	// The "-MM-DD" part of the string is fixed-length, so we split that way.
	y, err := strconv.Atoi(s[:len(s)-6])
	if err != nil {
		return unmarshalErr(s)
	}
	m, err := strconv.Atoi(s[len(s)-5 : len(s)-3])
	if err != nil {
		return unmarshalErr(s)
	}
	d, err := strconv.Atoi(s[len(s)-2:])
	if err != nil {
		return unmarshalErr(s)
	}
	*rd = Date(y, time.Month(m), d)
	return nil
}

type unmarshalErr string

func (e unmarshalErr) Error() string {
	return "ratadie.Day.Unmarshal: bad text format: " + string(e)
}

// IsLeapYear returns true if year is a Gregorian leap year.
func isLeapYear(year int) bool {
	return year%4 == 0 && (year%100 != 0 || year%400 == 0)
}
