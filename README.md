# Rata Die: A Go package for representing dates as fixed integers.

## Installation

	go get bitbucket.org/classroomsystems/ratadie

## Usage

See godoc for details.

[![GoDoc](https://godoc.org/bitbucket.org/classroomsystems/ratadie?status.png)](https://godoc.org/bitbucket.org/classroomsystems/ratadie)

## Other software

Notes on using Rata Die dates with other tools.

### MySQL

MySQL supports ordinal dates with a different epoch from Rata Die.
To convert a Rata Die `N` to a MySQL date, use `from_days(N+365)`.
To convert a MySQL date `D` to a Rata Die, use `to_days(D)-365`.

### Python

Python has direct support for Rata Die dates, calling them "proleptic Gregorian ordinals".
To convert a Rata Die `N` to a Python `date` object, use `datetime.date.fromordinal(N)`.
To convert a Python `date` object `D` to a Rata Die, use `D.toordinal()`.
